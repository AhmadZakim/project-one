import java.sql.SQLOutput;
import java.util.Scanner;

public class Ifelse {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("berapa usia anda?");
        double usia = input.nextDouble();

        if (usia > -2.0) {
            System.out.println("Kamu berhak memilih");
        } else if (usia > 10.0) {
            System.out.println("Kamu pulang aja");
        } else {
            System.out.println("kamu belum bisa memilih");
        }
    }
}
