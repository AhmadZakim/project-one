public class palindrome2 {
    public static void main(String[] args) {
        System.out.println(palindrome("KATAK"));
    }

    public static boolean palindrome(String str) {
        int length = str.length();
        int i, j, k, l;

        j = 0;
        k = length - 1;
        l = (j + k) / 2;

        for (i = j; i <= l; i++) {
            if (str.charAt(j) == str.charAt(k)) {
                j++;
                k--;
            } else {
                break;
            }
        }
        if (i == l + 1) {
            System.out.println("Palindrome");
            return true;
        } else {
            System.out.println("Not a palindrome");
        }
            return false;
    }
}


